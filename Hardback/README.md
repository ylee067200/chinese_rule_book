# 遊戲簡介
Hardback (精裝書) 是 Jeff Beck 與 Tim Fowers 合作的ㄧ套拼字遊戲，美術是由 Ryan Goldsberry 負責。這遊戲是 Domi-like，玩家借由自己的牌庫中隨機抽牌獲得字母；透過組合這些字母拼出ㄧ個字獲得獎勵，去取得分數或是獲得金錢購買更多的卡片。

# 檔案

```
${GAMES}/
    |
    |--- Rules/                            // 規則書
    |      |
    |      |--- Cards/                     // 卡片說明
    |      |      |
    |      |      |--- Variant/            // 遊戲變體規則
    |      |
    |      |--- Components.md              // 遊戲配件
    |      |
    |      |--- Rule.md                    // 遊戲規則
    |
    |--- References/                       // 遊戲資料
    |      |
    |      |--- Pictures/                  // 文件中用到的圖案
    |      |
    |      |--- "Hardback Rulebook.pdf"    // 遊戲規則書
    |
    |--- READMD.md                         // 本檔案
```