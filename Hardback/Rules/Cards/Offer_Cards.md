# 系列卡片 (Offer Cards)

## 卡片說明

系列卡片是遊戲的重心，系列卡片會有以下幾項資訊：
1. 字母、
2. 系列、
3. 價錢、
4. ㄧ般獎勵、與
5. 系列獎勵

![卡片](https://bitbucket.org/ylee067200/chinese_rule_book/raw/47b8df7119a1e97a7c2e84438f1082226550a67e/Hardback/Pictures/Offer_Cards.png)

## 長期經典卡片 (Timeless Classic Cards) 

長期經典卡片與ㄧ般系列卡片的差異除了是橫著擺之外，主要差異是長期經典卡片會一直留在玩家的面前，ㄧ直提供玩家獎勵，即便它不是拼字的一部分。

另一個差異是，放在玩家面前的長期經典卡片可供任何玩家在拼字時使用。ㄧ旦被其他玩家用來拼字，擁有的玩家就必須把被使用的長期經典卡片放進棄牌堆。

![長期經典卡片](https://bitbucket.org/ylee067200/chinese_rule_book/raw/47b8df7119a1e97a7c2e84438f1082226550a67e/Hardback/Pictures/timeless_cards.png)

***

# 系列

系列卡片在主遊戲有 4 種不同的系列：冒險系列 (指南針符號)、恐怖系列 (骷顱頭符號)、神祕系列 (放大鏡符號)、與羅曼史系列 (戀愛符號)。

ㄧ旦玩家用來拼字的卡片有 2 個以上是同一系列，玩家即可獲得那些卡片上的系列獎勵。系列獎勵的順序由玩家決定，而非字母排列的順序。

![系列](https://bitbucket.org/ylee067200/chinese_rule_book/raw/47b8df7119a1e97a7c2e84438f1082226550a67e/Hardback/Pictures/Offer_Card_Genres.png)

***

## 冒險系列獎勵

### 立享名聲 (Immediate Prestige)

![立享名聲](https://bitbucket.org/ylee067200/chinese_rule_book/raw/8ebb0ef47516bcf0ccc73c480c30bc13cc8aae84/Hardback/Pictures/Immediate_Prestige.png)
玩家在 **購買** 卡片的時候，立刻增加名聲。名聲增加的數量，依照卡片上箭頭所指的星星上面所列的數字。

### 移除這張牌 (Trash This Card)

![移除這張牌](https://bitbucket.org/ylee067200/chinese_rule_book/raw/8ebb0ef47516bcf0ccc73c480c30bc13cc8aae84/Hardback/Pictures/Trash_This_Card.png)
玩家在拼字之後，獲得這個系列獎勵的話，必須把這張牌移出遊戲。卡片移出遊戲之後，玩家可以獲得卡片上標註的金錢或是名聲。

***

## 恐怖系列獎勵

### 獲得墨水標記或是立可白標記 (Take Ink or Remover)

![獲得墨水標記或是立可白標記](https://bitbucket.org/ylee067200/chinese_rule_book/raw/8ebb0ef47516bcf0ccc73c480c30bc13cc8aae84/Hardback/Pictures/Take_Ink_or_Remover.png)
玩家在拼字之後，獲得這個系列獎勵的話，可以免費獲得 1 個墨水標記或是立可白標記。

### 獲得金錢或是名聲 (Gain Coin or Prestige)

![獲得金錢或是名聲](https://bitbucket.org/ylee067200/chinese_rule_book/raw/8ebb0ef47516bcf0ccc73c480c30bc13cc8aae84/Hardback/Pictures/Gain_Coin_or_Prestige.png)
玩家在拼字之後，獲得這個系列獎勵的話，可以獲得卡片上標示的金錢或是名聲。玩家必須只拿金錢或是名聲，不可各拿一部分。

***

## 神祕系列獎勵

### 發現周遭的萬用字 (Uncover Adjacent Wild)

![發現周遭的萬用字](https://bitbucket.org/ylee067200/chinese_rule_book/raw/8ebb0ef47516bcf0ccc73c480c30bc13cc8aae84/Hardback/Pictures/Uncover_Adjacent_Wild.png)
玩家在拼字之後，獲得這個系列獎勵的話，可以把這張牌左方或是右方的萬用卡翻開。玩家可以獲得翻開的萬用卡上的獎勵，包含系列獎勵。

### 綁架ㄧ張購賣區的卡片 (Jail Offer-Row Card)

![綁架ㄧ張購賣區的卡片](https://bitbucket.org/ylee067200/chinese_rule_book/raw/8ebb0ef47516bcf0ccc73c480c30bc13cc8aae84/Hardback/Pictures/Jail_Offer-Row_Card.png)
玩家在拼字之後，獲得這個系列獎勵的話，可以從購買區選 1 張卡片。被挑選的卡片，可以被丟棄到 (公眾區的) 廢牌碓，或是放在玩家前面成為玩家保留的購買卡。不管是哪一項，必須直接從供應牌庫最上方拿一張卡片，將購買區補齊 7 張卡片。

玩家只能綁架 1 張卡片。當你已經綁架 1 張卡片，你必須將之前的卡片丟棄到 (公眾區的) 廢牌碓，才能綁架新的卡片。

***

## 羅曼史系列獎勵

### 移除另一張牌 (Trash Another Card)

![移除另一張牌](https://bitbucket.org/ylee067200/chinese_rule_book/raw/8ebb0ef47516bcf0ccc73c480c30bc13cc8aae84/Hardback/Pictures/Trash_Another_Card.png)
玩家在拼字之後，獲得這個系列獎勵的話，可以把玩家廢牌碓中挑 1 張卡片並移出遊戲。移出卡片之後，玩家獲得卡片上的獎勵。

### 周遭卡片獎勵加倍 (Double Adjacent Card)

![周遭卡片獎勵加倍](https://bitbucket.org/ylee067200/chinese_rule_book/raw/8ebb0ef47516bcf0ccc73c480c30bc13cc8aae84/Hardback/Pictures/Double_Adjacent_Card.png)
玩家在拼字之後，獲得這個系列獎勵的話，可以把這張牌左方或是右方的卡片獎勵加倍。

玩家選擇的卡片必須不是覆蓋著。玩家若是有 1 張卡片，它左右兩邊都是有這個系列獎勵；而玩家兩個系列獎勵都選擇加倍這張卡片的話，玩家只能拿到 3 倍的獎勵，而非 4 倍。
